package com.example.ferret.loginpagetest;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.TypeTextAction;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import android.support.test.rule.ActivityTestRule;

import static android.support.test.espresso.Espresso.*;

import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class ExampleInstrumentedTest {

    String goodEmail = "123@1234.ru";
    String goodPassword = "1                 ";
    String youPassed = "Login successful";
    String invalidEmail = "Invalid email address";
    String shortPassword = "The provided password is too short";
    String incorrectPassword = "The provided password is invalid";


    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void PositiveOne() {
        onView(withId(R.id.Email)).perform(typeText(goodEmail));
        onView(withId(R.id.Password)).perform(typeText(goodPassword));
        onView(withId(R.id.loginBtn)).perform(click());
        onView(withText(youPassed)).check(matches(isDisplayed()));
    }

    @Test
    public void BadEmailAddress() {
        onView(withId(R.id.Email)).perform(typeText("afadsfasdfasdfasdf"));
        onView(withId(R.id.Password)).perform(typeText(goodPassword));
        onView(withId(R.id.loginBtn)).perform(click());
        onView(withText(invalidEmail)).check(matches(isDisplayed()));
    }

    @Test
    public void BadEmailWithSpecialChars() {
        onView(withId(R.id.Email)).perform(typeText("123@123@123.ru"));
        onView(withId(R.id.Password)).perform(typeText(goodPassword));
        onView(withId(R.id.loginBtn)).perform(click());
        onView(withText(invalidEmail)).check(matches(isDisplayed()));
    }

    @Test
    public void BadEmailWithBadHeader() {
        onView(withId(R.id.Email)).perform(typeText("@123.ru"));
        onView(withId(R.id.Password)).perform(typeText(goodPassword));
        onView(withId(R.id.loginBtn)).perform(click());
        onView(withText(invalidEmail)).check(matches(isDisplayed()));
    }

    @Test
    public void BadEmailWithBadFooter() {
        onView(withId(R.id.Email)).perform(typeText("123@1"));
        onView(withId(R.id.Password)).perform(typeText(goodPassword));
        onView(withId(R.id.loginBtn)).perform(click());
        onView(withText(invalidEmail)).check(matches(isDisplayed()));
    }

    @Test
    public void UnderLenghtPassword() {
        onView(withId(R.id.Email)).perform(typeText(goodEmail));
        onView(withId(R.id.Password)).perform(typeText("1234567"));
        onView(withId(R.id.loginBtn)).perform(click());
        onView(withText(shortPassword)).check(matches(isDisplayed()));
    }

    @Test
    public void SpacePassword() {
        onView(withId(R.id.Email)).perform(typeText(goodEmail));
        onView(withId(R.id.Password)).perform(typeText("                         "));
        onView(withId(R.id.loginBtn)).perform(click());
        onView(withText(incorrectPassword)).check(matches(isDisplayed()));
    }

    @Test
    public void BadPasswordBadLogin() {
        onView(withId(R.id.Email)).perform(typeText("1@1"));
        onView(withId(R.id.Password)).perform(typeText("                         "));
        onView(withId(R.id.loginBtn)).perform(click());
        onView(withText(invalidEmail + "\n" + incorrectPassword)).check(matches(isDisplayed()));
    }


}
