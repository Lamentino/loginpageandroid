package com.example.ferret.loginpagetest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button loginButton;
    private EditText emailField;
    private EditText passwordField;
    private TextView resultMsg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginButton = (Button) findViewById(R.id.loginBtn);
        emailField = (EditText) findViewById(R.id.Email);
        passwordField = (EditText) findViewById(R.id.Password);
        resultMsg = (TextView) findViewById(R.id.ResultMsg);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FinalMessageSet(PasswordCheck(passwordField.getText().toString()), LoginCheck(emailField.getText().toString()));
            }
        });
    }

    public String PasswordCheck(String passwordField) {

        if(passwordField.length()<8) {
            return "The provided password is too short";
        }
        else if (passwordField.replace(" ", "").length() == 0) {
            return "The provided password is invalid";
        }
        return "ok";
    }

    public String LoginCheck(String emailField) {
        String loginFailed = "Invalid email address";
        if (!emailField.contains("@"))
            return loginFailed;
        String[] separatedLogin = emailField.split("@");
        if (separatedLogin.length > 2)
            return loginFailed;
        if (!(separatedLogin[0].length() > 0))
            return loginFailed;
        if (!(separatedLogin[1].length() > 2))
            return loginFailed;
        return "ok";
    }

    public void FinalMessageSet(String passCheckResult, String loginCheckResult) {
        if (passCheckResult.equals("ok") && loginCheckResult.equals("ok"))
            resultMsg.setText("Login successful");
        else if (!passCheckResult.equals("ok") && !loginCheckResult.equals("ok"))
            resultMsg.setText(loginCheckResult + "\n" + passCheckResult);
        else if (!passCheckResult.equals("ok"))
            resultMsg.setText(passCheckResult);
        else if (!loginCheckResult.equals("ok"))
            resultMsg.setText(loginCheckResult);
        else
            resultMsg.setText("Что ты мать твою такое?!");
        resultMsg.setVisibility(View.VISIBLE);

    }

}
