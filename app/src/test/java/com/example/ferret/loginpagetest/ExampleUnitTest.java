package com.example.ferret.loginpagetest;

import android.widget.EditText;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    private MainActivity activity;
    private String LoginResult = "Invalid email address";

    @Before
    public void activity()
    {
        activity = new MainActivity();
    }

    @Test
    public void passwdCheckShort() {
        String res;
        res = activity.PasswordCheck("1234567");
        assertEquals(res, "The provided password is too short");
    }

    @Test
    public void passwdCheckSpace() {
        String res;
        res = activity.PasswordCheck("                               ");
        assertEquals(res, "The provided password is invalid");
    }

    @Test
    public void passwdCheckCorrect() {
        String res;
        res = activity.PasswordCheck("12345678");
        assertEquals(res, "ok");
    }

    @Test
    public void loginCheckNegative() {
        String res;
        res = activity.LoginCheck("12345678");
        assertEquals(res, LoginResult);
    }

    @Test
    public void loginCheckBeforeSizeNegative() {
        String res;
        res = activity.LoginCheck("@12345");
        assertEquals(res, LoginResult);
    }

    @Test
    public void loginCheckBeforeSizePositive() {
        String res;
        res = activity.LoginCheck("1@12345");
        assertEquals(res, "ok");
    }

    @Test
    public void loginCheckAfterSizeNegative() {
        String res;
        res = activity.LoginCheck("1@12");
        assertEquals(res, LoginResult);
    }

    @Test
    public void loginCheckAfterSizePositive() {
        String res;
        res = activity.LoginCheck("1@121");
        assertEquals(res, "ok");
    }

    @Test
    public void loginCheckSpecialChars() {
        String res;
        res = activity.LoginCheck("123@123@123");
        assertEquals(res, LoginResult);
    }

    @Test
    public void loginCheckBadEmail() {
        String res;
        res = activity.LoginCheck("1231241231");
        assertEquals(res, LoginResult);
    }

}